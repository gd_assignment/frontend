# Frontend

## External libs

- Next.js
- RTK Query for calling API endpoints and caching
- Tailwind CSS for basic look n' feel

## Install

1. Clone this repository
2. `npm install`
3. `npm run dev`

Then go to http://localhost:3000/available-products to see the application
