import { useState } from "react"
import Link from "next/link";

import AppMenuCategory from "./AppMenuCategory"

export default function AppLayout({children}) {
    const [isMenuVisible, setMenuVisible] = useState(true)

    return (
        <div id="main-container" className="flex flex-col w-full">
            <div id="headline" className="flex justify-between h-12 items-center text-2xl px-4 border-b bg-teal-700 text-teal-50">
                <span id="hamburger" onClick={() => setMenuVisible(!isMenuVisible)}>≡</span>
                <span id="title"><Link href="/">Inventory</Link></span>
                <span id="profile"></span>
            </div>
            <div id="menu-and-content" className="flex flex-col md:flex-row h-screen">
                { isMenuVisible &&
                <nav id="menu" className="p-4 w-full md:w-52 flex-col items-center h-fit md:h-full border-b md:border-r border-teal-700">
                    <ul className="flex flex-col items-center md:items-start">
                        <AppMenuCategory name="Shop"></AppMenuCategory>
                        <li><Link href="/available-products">Available products</Link></li>
                        <AppMenuCategory name="Admin"></AppMenuCategory>
                        <li>Products</li>
                        <li>Articles</li>
                        <li>Components</li>
                    </ul>
                </nav>
                }
                <main className="p-4 h-full w-full overflow-scroll">
                    {children}
                </main>
            </div>
        </div>
    )
}
