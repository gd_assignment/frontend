export default function AppMenuCategory({name}) {
  return (
    <li className="py-1 mt-1 text-xs font-bold uppercase text-teal-600">
      {name}
    </li>
  );
}
