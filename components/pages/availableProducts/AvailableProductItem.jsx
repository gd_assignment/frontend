import { useSellProductMutation } from "../../../features/api/availableProductsApi"

export default function AvailableProductItem({product}) {
    const [sellProduct, updateResult] = useSellProductMutation();

    const sellClicked = (itemId) => {
        let payload = {amount: 1};
        sellProduct({id: itemId, payload})
    };

    return (
        <div className="flex items-center justify-between h-12 p-2 border border-teal-700 mt-2 rounded-sm">
            <span id="product-name">{product.name} - Max amount: {product.max_amount}</span>
            <div className="flex items-center">
                <span id="product-price" className="mr-4">{product.price} {product.price_currency}</span>
                <button onClick={() => {sellClicked(product.id)}} className="py-2 px-4 bg-teal-700 text-white rounded-md">Sell</button>
            </div>
        </div>
    )
}