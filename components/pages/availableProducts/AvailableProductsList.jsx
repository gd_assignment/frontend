import AvailableProductItem from "./AvailableProductItem"

export default function AvailableProductsList({items}) {
    return (
        <div className="flex flex-col w-full">
            {items.map((product) => (<AvailableProductItem product={product} />))}
        </div>
    )
}
