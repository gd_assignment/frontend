export default function Spinner({ text = "Loading, please wait..." }) {
  return (
    <>
      <div className="p-2 flex items-center">
        <div className="mx-2 animate-spin border-t-transparent border-solid rounded-full border-teal-700 border-4 h-8 w-8"></div>
        <span>{text}</span>
      </div>
    </>
  );
};
