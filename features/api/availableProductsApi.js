import { apiSlice } from "./apiSlice";

function buildTagProviderList(result) {
  return result
    ? [
        ...result.map(({ pk: id }) => ({ type: "AvailableProduct", id })),
        { type: "AvailableProduct", id: "LIST" },
      ]
    : [{ type: "AvailableProduct", id: "LIST" }];
}

const availableProductsApi = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getAvailableProducts: builder.query({
      query: () => "/available_products/",
      providesTags: (result) => buildTagProviderList(result),
    }),
    sellProduct: builder.mutation({
      query: ({ id, payload }) => ({
        url: `/available_products/${id}/sell/`,
        method: "PATCH",
        body: payload,
      }),
      invalidatesTags: [{ type: "AvailableProduct", id: "LIST" }],
    }),
  }),
  overrideExisting: true,
});

export const {
  useGetAvailableProductsQuery,
  useSellProductMutation,
} = availableProductsApi;
