import '../styles/globals.css'
import store from '../store/store';
import { Provider } from 'react-redux';
import AppLayout from '../components/layout/AppLayout'


export default function App({ Component, pageProps }) {
  return <Provider store={store}><AppLayout><Component {...pageProps} /></AppLayout></Provider>
}
