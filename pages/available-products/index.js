import Head from 'next/head'

import Spinner from '../../components/pages/common/Spinner';
import AvailableProductsList from '../../components/pages/availableProducts/AvailableProductsList';

import { useGetAvailableProductsQuery } from '../../features/api/availableProductsApi'

export default function AvailableProducts() {
  const {
    data: availableProducts,
    isFetching,
    isSuccess,
    isError,
    error,
  } = useGetAvailableProductsQuery(undefined, { refetchOnMountOrArgChange: true });

  let content = '';

  if (isFetching) {
    content = <Spinner />;
  } else if (isSuccess) {
    content = <AvailableProductsList items={availableProducts} />;
  } else if (isError) {
    content = <span>Error: {error?.data?.detail}</span>;
  }

  return (
    <>
      <Head>
        <title>Inventory - Available products</title>
      </Head>
      <div className='flex flex-col w-full'>
        <div className='text-3xl'>Available products</div>
        <div className='w-full'>{content}</div>
      </div>
    </>
  )
}
