import Head from 'next/head'

export default function Home() {
  return (
    <>
      <Head>
        <title>Inventory</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className='flex flex-col w-full'>
        <div className='text-3xl'>Hello</div>
      </main>
    </>
  )
}
